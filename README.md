# Documentação da API

Dentro da pasta `docs` existem dois arquivos que documentam de forma simples a API.

- insomnia.json
- postman.json

Ambos são arquivos exportados de aplicativos de terceiros {[Postman](https://www.postman.com/) e [Insomnia](https://insomnia.rest/) }, e podem ser importados e utilizados como forma de documentação.

# Decições técnicas

## Tecnologias utilizadas

- Typescript
- Express
- Sequelize

O projeto conta com configurações de `linter` e `formatter` utilizando ESLint e Prettier, além de ter configurado o `husky` para capturar eventos do git (pre-commit e pre-push) e executar comandos de forma mais atomatizada em tempo de desenvolvimento do projeto.

Para produção escolhi usar o banco `mysql` e para desenvolvimento e testes optei pelo `sqlite`.

As migrations e o mapeamento de entitades do banco foi desenvolvido utilizando o `sequelize`.

Para o desenvolvimento dos testes foi utilizado o `jest`, juntamente com algumas libs utilitarias como o `faker` para gerar dados aleatórios a nivel de testes.

## Arquitetura limpa

Para execução do projeto eu optei por seguir alguns conceitos estudados em `Clean Architecture`.
Entendo que para projetos pequenos como MVPs ou aplicações de testes, não faz muito sentido optar por usar essa arquitetura, dado sua complexidade e aumento grande de arquivos para resolver um pequeno problema, porém a escolha de usar essa arquitetura foi por propósitos unicamente de estudo e aperfeiçoamento e solidificação desses conceitos, dado sua importância em cenários de grandes aplicações.

## Testes de integração

Optei por focar mais em testes de integração, dado que tais testes conseguem cobrir e validar de forma mais acertiva funcionalidades em um contexto geral do sistema.

## Migrations

Ao executar a aplicação, automaticamente o sistema limpa o banco e recria ele novamente utilizando as `migrations`. Isso obviamente não é uma opção viável para se usar em ambiente de produção, porém apenas para simplificar o desenvolvimento e execução do sistema, optei por deixar dessa forma, pois assim não é preciso previamente executar as migrations para a aplicação funcionar corretamente.

# Executando o projeto

## Docker Compose

```
  docker-compose up
```

## Manualmente

1. Instalar dependências

```
  yarn install
```

2. Configurar variáveis de ambiente

Criar um arquivo chamado `.env` na raiz do projeto, seguindo o modelo do arquivo `.env.example` porém preenchendos os valores.
Seguindo nessa linha você deverá previamente ter um banco mysql configurado no seu computador para conseguir executar o projeto em modo de `produção`, caso contrário, você consegue executar o projeto utilizando o `sqlite` que é um banco em arquivo que fica salvo no seu computador.

3. Buildando e executando

Depois de finalizar todos os passos anteriores de configuração do projeto basta buildar e executar o app:

```
  yarn build
  yarn start
```

### Possíveis problemas

- **The engine "node" is incompatible with this module. Expected version "14"**

O projeto foi desenvolvido utilizando o node 14, logo para manter compatibilidade eu decidi bloquear outras versões de node. Para resolver esse problema basta executar a aplicação com a versão 14 do nodejs. Isso pode facilmente ser feito utilizando o [nvm](https://github.com/nvm-sh/nvm).

- **SequelizeConnectionRefusedError: connect ECONNREFUSED 127.0.0.1:3306**

Verifique se configurou as variáveis de ambiente corretamente, esse erro ocorre quando a aplicação não conseguiu se conectar como banco de dados.
