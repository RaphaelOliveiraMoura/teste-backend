import supertest from 'supertest';

import { Genre, Movie } from '@/domain/models';
import { setupApplication } from '@/main/app';
import * as Builders from '@/tests/utils/builders';
import { request } from '@/tests/utils/request-matcher';

let app = null;

describe('UseCase: List Movies', () => {
  beforeEach(async () => {
    app = await setupApplication();
  });

  test('should list movies', async () => {
    const moviesAmmount = 5;

    await Promise.all(
      Array(moviesAmmount)
        .fill(0)
        .map(async () => {
          await Builders.createMovie();
        })
    );

    const response = request(await supertest(app).get('/movies').send()).expect(
      {
        status: 200,
        body: expect.arrayContaining([
          {
            id: expect.any(String),
            title: expect.any(String),
            genre: expect.arrayContaining([]),
          },
        ]),
      }
    );

    expect(response.body.length).toBe(moviesAmmount);
  });

  test('should test movies filters', async () => {
    const actors = await Builders.createActors();
    const directors = await Builders.createDirectors();

    const movies = [
      {
        title: 'Vingadores Ultimato',
        genre: [Genre.Action, Genre.Fantasy],
        director: { name: directors.AnthonyRusso.name },
        actors: [{ id: actors.RobertDowneyJr.id }],
      },
      {
        title: 'Star Wars',
        genre: [Genre.Action, Genre.Fantasy],
        director: { name: directors.GeorgeLucas.name },
        actors: [{ id: actors.AdamSandler.id }],
      },
      {
        title: 'Homem de Ferro',
        genre: [Genre.Action],
        director: { name: directors.AnthonyRusso.name },
        actors: [{ id: actors.RobertDowneyJr.id }],
      },
      {
        title: 'Homem de Ferro 2',
        genre: [Genre.Action],
        director: { name: directors.AnthonyRusso.name },
        actors: [{ id: actors.RobertDowneyJr.id }],
      },
      {
        title: 'Coringa',
        genre: [Genre.Mistery, Genre.Crime],
        director: { name: directors.ToddPhillips.name },
        actors: [{ id: actors.BradPitt.id }],
      },
      {
        title: 'Toy Story 4',
        genre: [Genre.Satire],
        director: { name: directors.JohnLasseter.name },
        actors: [{ id: actors.WillSmith.id }, { id: actors.BradPitt.id }],
      },
      {
        title: 'Harry Potter',
        genre: [Genre.Adventure, Genre.Mistery],
        director: { name: directors.DavidYates.name },
        actors: [{ id: actors.RobertDowneyJr.id }],
      },
    ];

    await Promise.all(
      movies.map((movie) => Builders.createMovie(movie as Movie))
    );

    const testCases: Array<{
      query: any;
      length: number;
      titles: string[];
    }> = [
      {
        query: { title: 'ferro' },
        length: 2,
        titles: ['Homem de Ferro', 'Homem de Ferro 2'],
      },
      {
        query: { genre: [Genre.Fantasy, Genre.Action] },
        length: 2,
        titles: ['Star Wars', 'Vingadores Ultimato'],
      },
      {
        query: { genre: [Genre.Crime] },
        length: 1,
        titles: ['Coringa'],
      },
      {
        query: { director: 'Ant' },
        length: 3,
        titles: ['Vingadores Ultimato', 'Homem de Ferro', 'Homem de Ferro 2'],
      },
      {
        query: { actors: ['Will Smith'] },
        length: 1,
        titles: ['Toy Story 4'],
      },
      {
        query: { actors: ['Will Smith', 'Brad'] },
        length: 1,
        titles: ['Toy Story 4'],
      },
      {
        query: {
          genre: [Genre.Action],
          director: 'anthony',
          title: 'ferro 2',
          actors: ['robert'],
        },
        length: 1,
        titles: ['Homem de Ferro 2'],
      },
    ];

    await Promise.all(
      testCases.map(async ({ query, length, titles = [] }) => {
        const response = await supertest(app)
          .get('/movies')
          .query(query)
          .send();

        const expectedArray = titles.map((title) => ({
          title,
          id: expect.any(String),
          genre: expect.arrayContaining([]),
        }));

        expect(response.body.length).toBe(length);
        expect(response.body).toStrictEqual(
          expect.arrayContaining(expectedArray)
        );
      })
    );
  });
});
