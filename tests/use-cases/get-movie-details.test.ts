import supertest from 'supertest';

import { Genre } from '@/domain/models';
import { setupApplication } from '@/main/app';
import * as Builders from '@/tests/utils/builders';
import { request } from '@/tests/utils/request-matcher';

let app = null;

describe('UseCase: Get Movie Details', () => {
  beforeEach(async () => {
    app = await setupApplication();
  });

  test('should get movie details', async () => {
    const admin = await Builders.createUser({ isAdmin: true });
    const jwt = await Builders.jwt(app, admin);

    const actors = await Builders.createActors();

    const response = await supertest(app)
      .post('/movies')
      .set('authorization', jwt)
      .send({
        title: 'Os Vingadores',
        description: 'Melhor filme que tem',
        genre: [Genre.Action],
        actors: [
          actors.RobertDowneyJr,
          actors.BradPitt,
          { name: 'new actor' },
          { name: 'new actor 2' },
        ],
        releaseDate: new Date().toISOString(),
        director: { name: 'director name' },
      });

    const { id: movieId } = response.body;

    request(await supertest(app).get(`/movies/${movieId}`).send()).expect({
      status: 200,
      body: {
        id: expect.any(String),
        title: 'Os Vingadores',
        description: 'Melhor filme que tem',
        genre: expect.arrayContaining(['Action']),
        actors: expect.arrayContaining([
          { id: '1', name: 'Robert Downey Jr.' },
          { id: '3', name: 'Brad Pitt' },
          { id: expect.any(String), name: 'new actor' },
          { id: expect.any(String), name: 'new actor 2' },
        ]),
        director: { id: expect.any(String), name: 'director name' },
        ratingAvarage: 0,
        releaseDate: expect.toBeDate(),
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });
  });

  test('should return 400 when try get a invalid movie', async () => {
    request(await supertest(app).get(`/movies/${'invalid-id'}`).send()).expect({
      status: 400,
      body: {
        error: 'movie does not exists',
      },
    });
  });
});
