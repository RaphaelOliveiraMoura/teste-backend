import supertest from 'supertest';

import { setupApplication } from '@/main/app';
import * as Builders from '@/tests/utils/builders';
import { request } from '@/tests/utils/request-matcher';

let app = null;

describe('UseCase: Rate Movie', () => {
  beforeEach(async () => {
    app = await setupApplication();
  });

  test('user should rate a movie', async () => {
    const movie = await Builders.createMovie();

    const user1 = await Builders.createUser({ isAdmin: false });
    const jwt1 = await Builders.jwt(app, user1);

    request(
      await supertest(app)
        .post(`/movies/${movie.id}/rate`)
        .set('authorization', jwt1)
        .send({ rating: 4 })
    ).expect({
      status: 200,
      body: {
        id: movie.id,
        title: movie.title,
        description: movie.description,
        ratingAvarage: 4,
        director: {
          id: expect.any(String),
          name: expect.any(String),
        },
        genre: expect.arrayContaining([]),
        actors: expect.arrayContaining([]),
        releaseDate: expect.toBeDate(),
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });

    request(
      await supertest(app)
        .post(`/movies/${movie.id}/rate`)
        .set('authorization', jwt1)
        .send({ rating: 1 })
    ).expect({
      status: 200,
      body: {
        id: movie.id,
        title: movie.title,
        description: movie.description,
        ratingAvarage: 1,
        director: {
          id: expect.any(String),
          name: expect.any(String),
        },
        genre: expect.arrayContaining([]),
        actors: expect.arrayContaining([]),
        releaseDate: expect.toBeDate(),
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });

    const user2 = await Builders.createUser();
    const jwt2 = await Builders.jwt(app, user2);

    request(
      await supertest(app)
        .post(`/movies/${movie.id}/rate`)
        .set('authorization', jwt2)
        .send({ rating: 4 })
    ).expect({
      status: 200,
      body: {
        id: movie.id,
        title: movie.title,
        description: movie.description,
        ratingAvarage: 2.5,
        director: {
          id: expect.any(String),
          name: expect.any(String),
        },
        genre: expect.arrayContaining([]),
        actors: expect.arrayContaining([]),
        releaseDate: expect.toBeDate(),
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });
  });

  test('should return 401 when admin try rate a movie', async () => {
    const movie = await Builders.createMovie();

    const admin = await Builders.createUser({ isAdmin: true });
    const jwt1 = await Builders.jwt(app, admin);

    request(
      await supertest(app)
        .post(`/movies/${movie.id}/rate`)
        .set('authorization', jwt1)
        .send({ rating: 4 })
    ).expect({
      status: 401,
      body: {
        error: 'you dont have permission to this action',
      },
    });
  });

  test('should return 400 when user try rate a invalid movie', async () => {
    const admin = await Builders.createUser({ isAdmin: true });
    const jwt1 = await Builders.jwt(app, admin);

    request(
      await supertest(app)
        .post(`/movies/${'invalid-movie'}/rate`)
        .set('authorization', jwt1)
        .send({ rating: 4 })
    ).expect({
      status: 400,
      body: {
        error: 'movie does not exists',
      },
    });
  });

  test('should return 400 when send invalid payload', async () => {
    const user = await Builders.createUser();
    const jwt = await Builders.jwt(app, user);
    const movie = await Builders.createMovie();

    const testCases = [
      [{}, 'rating is required'],
      [{ rating: 10 }, 'rating must be one of types [0,1,2,3,4]'],
    ];

    await Promise.all(
      testCases.map(async ([payload, errorMessage]) => {
        request(
          await supertest(app)
            .post(`/movies/${movie.id}/rate`)
            .set('authorization', jwt)
            .send(payload)
        ).expect({
          status: 400,
          body: { error: errorMessage },
        });
      })
    );
  });
});
