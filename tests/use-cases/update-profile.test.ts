import supertest from 'supertest';

import { setupApplication } from '@/main/app';
import * as Builders from '@/tests/utils/builders';
import { request } from '@/tests/utils/request-matcher';

let app = null;

describe('UseCase: Update Profile', () => {
  beforeEach(async () => {
    app = await setupApplication();
  });

  test('should update username', async () => {
    const user = await Builders.createUser();

    const jwt = await Builders.jwt(app, user);

    request(
      await supertest(app).put('/users').set('authorization', jwt).send({
        name: 'updated-name',
      })
    ).expect({
      status: 200,
      body: {
        id: user.id,
        email: user.email,
        name: 'updated-name',
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
        isAdmin: user.isAdmin,
      },
    });
  });

  test('should update password', async () => {
    const user = await Builders.createUser();

    const jwt = await Builders.jwt(app, user);

    request(
      await supertest(app).put('/users').set('authorization', jwt).send({
        password: 'updated-password',
      })
    ).expect({
      status: 200,
      body: {
        id: user.id,
        email: user.email,
        name: user.name,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
        isAdmin: user.isAdmin,
      },
    });

    await supertest(app)
      .post('/signin')
      .send({
        email: user.email,
        password: 'updated-password',
      })
      .expect(200);
  });

  test('should return 400 for invalid password', async () => {
    const user = await Builders.createUser();

    const jwt = await Builders.jwt(app, user);

    request(
      await supertest(app).put('/users').set('authorization', jwt).send({
        password: 'invalid',
      })
    ).expect({
      status: 400,
      body: {
        error: 'password must have at least 8 characters',
      },
    });
  });

  test('should not be able to update unauthorized properties', async () => {
    const user = await Builders.createUser();

    const jwt = await Builders.jwt(app, user);

    request(
      await supertest(app).put('/users').set('authorization', jwt).send({
        isAdmin: true,
        email: 'updated-email@gmail.com',
        id: 'random-id',
        createdAt: true,
        updatedAt: true,
      })
    ).expect({
      status: 200,
      body: {
        id: user.id,
        email: user.email,
        name: user.name,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
        isAdmin: user.isAdmin,
      },
    });
  });
});
