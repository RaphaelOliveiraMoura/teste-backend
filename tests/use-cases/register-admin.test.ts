import supertest from 'supertest';

import { setupApplication } from '@/main/app';
import * as Builders from '@/tests/utils/builders';
import { request } from '@/tests/utils/request-matcher';

let app = null;

describe('UseCase: Register Admin', () => {
  beforeEach(async () => {
    app = await setupApplication();
  });

  test('should register a new admin', async () => {
    request(
      await supertest(app).post('/admin').send({
        name: 'username',
        email: 'email@gmail.com',
        password: 'my-secret-password',
        confirmPassword: 'my-secret-password',
      })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        email: 'email@gmail.com',
        name: 'username',
        isAdmin: true,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });
  });

  test('should not register a admin with a already used email', async () => {
    const user = await Builders.createUser({ isAdmin: false });

    request(
      await supertest(app).post('/admin').send({
        name: 'username',
        email: user.email,
        password: 'my-secret-password',
        confirmPassword: 'my-secret-password',
      })
    ).expect({
      status: 400,
      body: {
        error: 'user already exists',
      },
    });
  });

  test('should not register user without admin when send admin property as false at body', async () => {
    request(
      await supertest(app).post('/admin').send({
        name: 'username',
        email: 'email@gmail.com',
        password: 'my-secret-password',
        confirmPassword: 'my-secret-password',
        isAdmin: false,
      })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        email: 'email@gmail.com',
        name: 'username',
        isAdmin: true,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });
  });

  test('should get status 400 when try register a new user with invalid payload', async () => {
    const validPayload = {
      name: 'username',
      email: 'email@gmail.com',
      password: 'my-secret-password',
      confirmPassword: 'my-secret-password',
    };

    const testCases = [
      [{ ...validPayload, name: undefined }, 'name is required'],
      [{ ...validPayload, email: undefined }, 'email is required'],
      [{ ...validPayload, password: undefined }, 'password is required'],
      [
        { ...validPayload, confirmPassword: undefined },
        'password and confirmPassword unmatch',
      ],
      [
        { ...validPayload, confirmPassword: 'diferent-password' },
        'password and confirmPassword unmatch',
      ],
      [{ ...validPayload, email: 'invalid-email' }, 'email is invalid'],
    ];

    await Promise.all(
      testCases.map(async ([payload, errorMessage]) => {
        request(await supertest(app).post('/admin').send(payload)).expect({
          status: 400,
          body: { error: errorMessage },
        });
      })
    );
  });
});
