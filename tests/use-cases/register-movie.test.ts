import supertest from 'supertest';

import { Genre } from '@/domain/models';
import { setupApplication } from '@/main/app';
import * as Builders from '@/tests/utils/builders';
import { request } from '@/tests/utils/request-matcher';

let app = null;

describe('UseCase: Register Movie', () => {
  beforeEach(async () => {
    app = await setupApplication();
  });

  test('should register a new movie as admin with an uncreated director', async () => {
    const actors = await Builders.createActors();

    const admin = await Builders.createUser({ isAdmin: true });
    const jwt = await Builders.jwt(app, admin);

    request(
      await supertest(app)
        .post('/movies')
        .set('authorization', jwt)
        .send({
          title: 'Os Vingadores',
          description: 'Melhor filme que tem',
          genre: [Genre.Action],
          actors: [
            actors.RobertDowneyJr,
            actors.BradPitt,
            { name: 'new actor' },
            { name: 'new actor 2' },
          ],
          director: {
            name: 'director name',
          },
          releaseDate: new Date().toISOString(),
        })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        title: 'Os Vingadores',
        description: 'Melhor filme que tem',
        genre: expect.arrayContaining(['Action']),
        actors: expect.arrayContaining([
          { id: '1', name: 'Robert Downey Jr.' },
          { id: '3', name: 'Brad Pitt' },
          { id: expect.any(String), name: 'new actor' },
          { id: expect.any(String), name: 'new actor 2' },
        ]),
        director: {
          id: expect.any(String),
          name: 'director name',
        },
        ratingAvarage: 0,
        releaseDate: expect.toBeDate(),
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });
  });

  test('should register a new movie as admin with an created director', async () => {
    const directors = await Builders.createDirectors();
    const actors = await Builders.createActors();

    const admin = await Builders.createUser({ isAdmin: true });
    const jwt = await Builders.jwt(app, admin);

    request(
      await supertest(app)
        .post('/movies')
        .set('authorization', jwt)
        .send({
          title: 'Os Vingadores',
          description: 'Melhor filme que tem',
          genre: [Genre.Action],
          actors: [
            actors.RobertDowneyJr,
            actors.BradPitt,
            { name: 'new actor' },
            { name: 'new actor 2' },
          ],
          director: {
            id: directors.JohnLasseter.id,
            name: directors.JohnLasseter.name,
          },
          releaseDate: new Date().toISOString(),
        })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        title: 'Os Vingadores',
        description: 'Melhor filme que tem',
        genre: expect.arrayContaining(['Action']),
        actors: expect.arrayContaining([
          { id: '1', name: 'Robert Downey Jr.' },
          { id: '3', name: 'Brad Pitt' },
          { id: expect.any(String), name: 'new actor' },
          { id: expect.any(String), name: 'new actor 2' },
        ]),
        director: {
          id: directors.JohnLasseter.id,
          name: directors.JohnLasseter.name,
        },
        ratingAvarage: 0,
        releaseDate: expect.toBeDate(),
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });
  });

  test('should not be able register movie as user', async () => {
    const user = await Builders.createUser({ isAdmin: false });
    const jwt = await Builders.jwt(app, user);

    request(
      await supertest(app)
        .post('/movies')
        .set('authorization', jwt)
        .send({
          title: 'Os Vingadores',
          description: 'Melhor filme que tem',
          genre: [Genre.Action],
          actors: [],
          releaseDate: new Date().toISOString(),
          director: { name: 'director name' },
        })
    ).expect({
      status: 401,
      body: {
        error: 'you dont have permission to this action',
      },
    });
  });

  test('should return 400 to invalid payload', async () => {
    const admin = await Builders.createUser({ isAdmin: true });
    const jwt = await Builders.jwt(app, admin);

    const validPayload = {
      title: 'Os Vingadores',
      description: 'Melhor filme que tem',
      genre: [Genre.Action],
      actors: [],
      releaseDate: new Date().toISOString(),
      director: { name: 'director name' },
    };

    const testCases = [
      [{ ...validPayload, title: undefined }, 'title is required'],
      [{ ...validPayload, description: undefined }, 'description is required'],
      [{ ...validPayload, genre: undefined }, 'genre is required'],
      [{ ...validPayload, actors: undefined }, 'actors is required'],
      [{ ...validPayload, releaseDate: undefined }, 'releaseDate is required'],
      [{ ...validPayload, genre: 'invalid-genre' }, 'genre must be array'],
      [{ ...validPayload, actors: 'invalid-actors' }, 'actors must be array'],
      [
        { ...validPayload, releaseDate: 'invalid-date' },
        'releaseDate must be date',
      ],
    ];

    await Promise.all(
      testCases.map(async ([payload, errorMessage]) => {
        request(
          await supertest(app)
            .post('/movies')
            .set('authorization', jwt)
            .send(payload)
        ).expect({
          status: 400,
          body: {
            error: errorMessage,
          },
        });
      })
    );
  });
});
