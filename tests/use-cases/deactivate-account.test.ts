import supertest from 'supertest';

import { setupApplication } from '@/main/app';
import * as Builders from '@/tests/utils/builders';
import { request } from '@/tests/utils/request-matcher';

let app = null;

describe('UseCase: Deactivate Account', () => {
  beforeEach(async () => {
    app = await setupApplication();
  });

  test('should deactivate a account', async () => {
    const user = await Builders.createUser();

    const jwt = await Builders.jwt(app, user);

    request(
      await supertest(app)
        .delete('/users/deactivate')
        .set('authorization', jwt)
        .send()
    ).expect({
      status: 200,
      body: {},
    });
  });

  test('should return 401 when try deactivate a invalid account', async () => {
    request(
      await supertest(app)
        .delete('/users/deactivate')
        .set('authorization', 'Bearer invalid-token')
        .send()
    ).expect({
      status: 401,
      body: { error: 'authorization Token is invalid' },
    });
  });
});
