import supertest from 'supertest';

import { setupApplication } from '@/main/app';
import * as Builders from '@/tests/utils/builders';
import { request } from '@/tests/utils/request-matcher';

let app = null;

describe('UseCase: Sign In', () => {
  beforeEach(async () => {
    app = await setupApplication();
  });

  test('should return token when user sign in', async () => {
    const user = await Builders.createUser();

    request(
      await supertest(app).post('/signin').send({
        email: user.email,
        password: user.password,
      })
    ).expect({
      status: 200,
      body: {
        token: expect.any(String),
        email: user.email,
        name: user.name,
      },
    });
  });

  test('should return 401 when pass invalid email', async () => {
    const user = await Builders.createUser();

    request(
      await supertest(app).post('/signin').send({
        email: 'invalid-email@gmail.com',
        password: user.password,
      })
    ).expect({
      status: 401,
      body: { error: 'invalid credentials' },
    });
  });

  test('should return 401 when pass invalid password', async () => {
    const user = await Builders.createUser();

    request(
      await supertest(app).post('/signin').send({
        email: user.email,
        password: 'invalid-password',
      })
    ).expect({
      status: 401,
      body: { error: 'invalid credentials' },
    });
  });

  test('should return 401 when try signin with a deactivate user', async () => {
    const user = await Builders.createUser();
    const jwt = await Builders.jwt(app, user);

    await supertest(app)
      .delete('/users/deactivate')
      .set('authorization', jwt)
      .send()
      .expect(200);

    request(
      await supertest(app).post('/signin').send({
        email: user.email,
        password: user.password,
      })
    ).expect({
      status: 401,
      body: { error: 'invalid credentials' },
    });
  });

  test('should return 400 when send invalid payload', async () => {
    const validPayload = {
      email: 'email@gmail.com',
      password: 'password123',
    };

    const testCases = [
      [{ ...validPayload, email: '' }, 'email is required'],
      [{ ...validPayload, password: '' }, 'password is required'],
      [{ ...validPayload, email: 'invalid-email' }, 'email is invalid'],
    ];

    await Promise.all(
      testCases.map(async ([payload, errorMessage]) => {
        request(await supertest(app).post('/signin').send(payload)).expect({
          status: 400,
          body: { error: errorMessage },
        });
      })
    );
  });
});
