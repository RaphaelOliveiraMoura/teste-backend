expect.extend({
  toBeDate(received) {
    return {
      message: () => `expected ${received} to be date`,
      pass: new Date(received).toString() !== 'Invalid Date',
    };
  },
});
