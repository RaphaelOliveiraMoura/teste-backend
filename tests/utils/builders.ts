import { Application } from 'express';
import faker from 'faker';
import supertest from 'supertest';

import { Director, Movie, User } from '@/domain/models';
import { BcryptEncrypter } from '@/infra/encrypter';
import { MovieRepository, UserRepository } from '@/infra/repositories';
import { ActorModel } from '@/infra/sequelize/models/actor';
import { DirectorModel } from '@/infra/sequelize/models/director';

export async function createUser(userData: Partial<User> = {}) {
  const repository = new UserRepository();

  const rawPassword = 'password123';

  const encrypter = new BcryptEncrypter();
  const encryptedPassowrd = await encrypter.encrypt(rawPassword);

  const user = await repository.insert({
    name: faker.name.firstName(),
    email: faker.internet.email(),
    isAdmin: false,
    password: encryptedPassowrd,
    ...userData,
  });

  return { ...user, password: rawPassword };
}

export async function createMovie(movieData: Partial<Movie> = {}) {
  const repository = new MovieRepository();

  const movie = await repository.insert({
    title: faker.commerce.productName(),
    description: faker.commerce.productDescription(),
    releaseDate: faker.date.past(),
    actors: [],
    genre: [],
    director: { name: faker.name.firstName() } as Director,
    ...movieData,
  });

  return movie;
}

export async function createActors() {
  const actorsMap = {
    RobertDowneyJr: {
      id: '1',
      name: 'Robert Downey Jr.',
    },
    WillSmith: {
      id: '2',
      name: 'Will Smith.',
    },
    BradPitt: {
      id: '3',
      name: 'Brad Pitt',
    },
    AdamSandler: {
      id: '4',
      name: 'Adam Sandler',
    },
  };

  const actors = Object.keys(actorsMap).map((key) => actorsMap[key]);

  await ActorModel.bulkCreate(actors);

  return actorsMap;
}

export async function createDirectors() {
  const directorsMap = {
    AnthonyRusso: {
      id: '1',
      name: 'Anthony Russo',
    },
    DavidYates: {
      id: '2',
      name: 'David Yates',
    },
    GeorgeLucas: {
      id: '3',
      name: 'George Lucas',
    },
    ToddPhillips: {
      id: '4',
      name: 'Todd Phillips',
    },
    JohnLasseter: {
      id: '5',
      name: 'John Lasseter',
    },
  };

  const directors = Object.keys(directorsMap).map((key) => directorsMap[key]);

  await DirectorModel.bulkCreate(directors);

  return directorsMap;
}

export async function jwt(
  app: Application,
  { email, password }: Partial<User> = {}
) {
  const response = await supertest(app)
    .post('/signin')
    .send({ email, password });

  return `Bearer ${response.body.token}`;
}
