import supertest from 'supertest';

export const request = (result: supertest.Response) => ({
  expect: ({ status, body }) => {
    expect(result.body).toStrictEqual(body);
    expect(result.status).toBe(status);
    return result;
  },
});
