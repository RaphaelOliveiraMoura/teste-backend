import supertest from 'supertest';

import { Genre } from '@/domain/models';
import { setupApplication } from '@/main/app';
import { request } from '@/tests/utils/request-matcher';

let app = null;

describe('Integration', () => {
  beforeAll(async () => {
    app = await setupApplication();
  });

  test('should test a complete integration flow', async () => {
    request(
      await supertest(app).post('/signin').send({
        email: 'user1@gmail.com',
        password: 'user123456',
      })
    ).expect({ status: 401, body: { error: 'invalid credentials' } });

    request(
      await supertest(app).post('/register').send({
        name: 'user1',
        email: 'user1@gmail.com',
        password: 'user123456',
        confirmPassword: 'user123456',
      })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        email: 'user1@gmail.com',
        name: 'user1',
        isAdmin: false,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });

    request(
      await supertest(app).post('/register').send({
        name: 'user2',
        email: 'user2@gmail.com',
        password: 'user123456',
        confirmPassword: 'user123456',
      })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        email: 'user2@gmail.com',
        name: 'user2',
        isAdmin: false,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });

    request(
      await supertest(app).post('/register').send({
        name: 'user3',
        email: 'user3@gmail.com',
        password: 'user123456',
        confirmPassword: 'user123456',
      })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        email: 'user3@gmail.com',
        name: 'user3',
        isAdmin: false,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });

    request(
      await supertest(app).post('/admin').send({
        name: 'admin1',
        email: 'admin1@gmail.com',
        password: 'admin123456',
        confirmPassword: 'admin123456',
      })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        email: 'admin1@gmail.com',
        name: 'admin1',
        isAdmin: true,
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });

    const { body: user1 } = await supertest(app).post('/signin').send({
      email: 'user1@gmail.com',
      password: 'user123456',
    });

    const { body: user2 } = await supertest(app).post('/signin').send({
      email: 'user2@gmail.com',
      password: 'user123456',
    });

    const { body: user3 } = await supertest(app).post('/signin').send({
      email: 'user3@gmail.com',
      password: 'user123456',
    });

    const { body: admin1 } = await supertest(app).post('/signin').send({
      email: 'admin1@gmail.com',
      password: 'admin123456',
    });

    const { body: movie } = request(
      await supertest(app)
        .post('/movies')
        .set('authorization', `Bearer ${admin1.token}`)
        .send({
          title: 'Homem Formiga 2',
          description: 'Descrição maneira',
          genre: [Genre.Action],
          actors: [
            { name: 'Raphael de Oliveira' },
            { name: 'Silvestre Stalone' },
          ],
          director: {
            name: 'Pietro Ferraz',
          },
          releaseDate: new Date().toISOString(),
        })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        title: 'Homem Formiga 2',
        description: 'Descrição maneira',
        genre: [Genre.Action],
        actors: expect.arrayContaining([
          { id: expect.any(String), name: 'Raphael de Oliveira' },
          { id: expect.any(String), name: 'Silvestre Stalone' },
        ]),
        director: {
          id: expect.any(String),
          name: 'Pietro Ferraz',
        },
        ratingAvarage: 0,
        releaseDate: expect.toBeDate(),
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });

    request(
      await supertest(app)
        .post(`/movies/${movie.id}/rate`)
        .set('authorization', `Bearer ${user1.token}`)
        .send({ rating: 1 })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        title: 'Homem Formiga 2',
        description: 'Descrição maneira',
        genre: [Genre.Action],
        actors: expect.arrayContaining([
          { id: expect.any(String), name: 'Raphael de Oliveira' },
          { id: expect.any(String), name: 'Silvestre Stalone' },
        ]),
        director: {
          id: expect.any(String),
          name: 'Pietro Ferraz',
        },
        ratingAvarage: 1,
        releaseDate: expect.toBeDate(),
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });

    request(
      await supertest(app)
        .post(`/movies/${movie.id}/rate`)
        .set('authorization', `Bearer ${user1.token}`)
        .send({ rating: 4 })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        title: 'Homem Formiga 2',
        description: 'Descrição maneira',
        genre: [Genre.Action],
        actors: expect.arrayContaining([
          { id: expect.any(String), name: 'Raphael de Oliveira' },
          { id: expect.any(String), name: 'Silvestre Stalone' },
        ]),
        director: {
          id: expect.any(String),
          name: 'Pietro Ferraz',
        },
        ratingAvarage: 4,
        releaseDate: expect.toBeDate(),
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });

    request(
      await supertest(app)
        .post(`/movies/${movie.id}/rate`)
        .set('authorization', `Bearer ${user2.token}`)
        .send({ rating: 2 })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        title: 'Homem Formiga 2',
        description: 'Descrição maneira',
        genre: [Genre.Action],
        actors: expect.arrayContaining([
          { id: expect.any(String), name: 'Raphael de Oliveira' },
          { id: expect.any(String), name: 'Silvestre Stalone' },
        ]),
        director: {
          id: expect.any(String),
          name: 'Pietro Ferraz',
        },
        ratingAvarage: 3,
        releaseDate: expect.toBeDate(),
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });

    request(
      await supertest(app)
        .post(`/movies/${movie.id}/rate`)
        .set('authorization', `Bearer ${user3.token}`)
        .send({ rating: 0 })
    ).expect({
      status: 200,
      body: {
        id: expect.any(String),
        title: 'Homem Formiga 2',
        description: 'Descrição maneira',
        genre: [Genre.Action],
        actors: expect.arrayContaining([
          { id: expect.any(String), name: 'Raphael de Oliveira' },
          { id: expect.any(String), name: 'Silvestre Stalone' },
        ]),
        director: {
          id: expect.any(String),
          name: 'Pietro Ferraz',
        },
        ratingAvarage: 2,
        releaseDate: expect.toBeDate(),
        createdAt: expect.toBeDate(),
        updatedAt: expect.toBeDate(),
      },
    });
  });
});
