import { config } from 'dotenv';

config();

export const env = {
  environment: (process.env.NODE_ENV || 'development') as
    | 'development'
    | 'production'
    | 'test',
  hasher_secret: process.env.HASHER_SECRET || 'HASHER_SECRET',
};
