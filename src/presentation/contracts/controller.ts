import { HttpRequest, HttpResponse } from '@/presentation/contracts';

export interface Controller {
  handle: (httpRequest: HttpRequest) => Promise<HttpResponse>;
  handleError: (error: Error) => Promise<HttpResponse>;
}
