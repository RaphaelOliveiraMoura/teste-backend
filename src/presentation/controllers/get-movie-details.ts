import { MovieNotExistsError } from '@/domain/errors';
import { GetMovieDetails, GetMovieDetailsUseCase } from '@/domain/use-cases';
import {
  Controller,
  HttpRequest,
  HttpResponse,
  HttpResponseError,
  ok,
  serverError,
  badRequest,
} from '@/presentation/contracts';

export class GetMovieDetailsController implements Controller {
  constructor(private readonly usecase: GetMovieDetailsUseCase) {}

  async handle(
    httpRequest: HttpRequest<GetMovieDetails.Params>
  ): Promise<HttpResponse<GetMovieDetails.Result | HttpResponseError>> {
    const { movieId } = httpRequest.params;

    const movie = await this.usecase.get({ movieId });

    return ok(movie);
  }

  async handleError(error: Error): Promise<HttpResponse> {
    if (error instanceof MovieNotExistsError) return badRequest(error);
    return serverError(error);
  }
}
