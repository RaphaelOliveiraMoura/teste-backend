import {
  InvalidAuthorizationError,
  MovieNotExistsError,
} from '@/domain/errors';
import {
  RateMovieUseCase,
  RateMovie,
  AuthorizationUseCase,
} from '@/domain/use-cases';
import {
  badRequest,
  Controller,
  HttpRequest,
  HttpResponse,
  HttpResponseError,
  ok,
  serverError,
  unauthorized,
} from '@/presentation/contracts';
import { BodyValidationError } from '@/presentation/errors';
import { Validation } from '@/validation/contracts';

export class RateMovieController implements Controller {
  constructor(
    private readonly usecase: RateMovieUseCase,
    private readonly validator: Validation,
    private readonly authenticator: AuthorizationUseCase
  ) {}

  async handle(
    httpRequest: HttpRequest<RateMovie.Params>
  ): Promise<HttpResponse<RateMovie.Result | HttpResponseError>> {
    const { authorization } = httpRequest.headers;
    const user = await this.authenticator.authorize({ authorization });

    const error = this.validator.validate(httpRequest.body);

    if (error) throw new BodyValidationError(error);

    const { movieId } = httpRequest.params;
    const { rating } = httpRequest.body;

    const movie = await this.usecase.rate({
      user: {
        id: user.id,
        isAdmin: user.isAdmin,
      },
      movieId,
      rating,
    });

    return ok(movie);
  }

  async handleError(error: Error): Promise<HttpResponse> {
    if (error instanceof InvalidAuthorizationError) return unauthorized(error);
    if (error instanceof BodyValidationError) return badRequest(error);
    if (error instanceof MovieNotExistsError) return badRequest(error);
    return serverError(error);
  }
}
