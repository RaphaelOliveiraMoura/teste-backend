import { ListMovies, ListMoviesUseCase } from '@/domain/use-cases';
import {
  Controller,
  HttpRequest,
  HttpResponse,
  HttpResponseError,
  ok,
  serverError,
} from '@/presentation/contracts';

type ListMoviesControllerResponse = Array<{
  id: string;
  title: string;
  genre: string[];
}>;

export class ListMoviesController implements Controller {
  constructor(private readonly usecase: ListMoviesUseCase) {}

  async handle(
    httpRequest: HttpRequest<ListMovies.Params>
  ): Promise<HttpResponse<ListMoviesControllerResponse | HttpResponseError>> {
    const movies = await this.usecase.get({ filters: httpRequest.query });

    return ok(movies.map(({ id, title, genre }) => ({ id, title, genre })));
  }

  async handleError(error: Error): Promise<HttpResponse> {
    return serverError(error);
  }
}
