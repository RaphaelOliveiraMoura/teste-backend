import { UserAlreadyExistsError } from '@/domain/errors';
import { RegisterAdmin, RegisterAdminUseCase } from '@/domain/use-cases';
import {
  badRequest,
  Controller,
  HttpRequest,
  HttpResponse,
  HttpResponseError,
  ok,
  serverError,
} from '@/presentation/contracts';
import { BodyValidationError } from '@/presentation/errors';
import { Validation } from '@/validation/contracts';

type RegisterUserControllerResponse = {
  id: string;
  name: string;
  email: string;
  createdAt: Date;
  updatedAt: Date;
  isAdmin: boolean;
};

export class RegisterAdminController implements Controller {
  constructor(
    private readonly usecase: RegisterAdminUseCase,
    private readonly validator: Validation
  ) {}

  async handle(
    httpRequest: HttpRequest<RegisterAdmin.Params>
  ): Promise<HttpResponse<RegisterUserControllerResponse | HttpResponseError>> {
    const error = this.validator.validate(httpRequest.body);

    if (error) throw new BodyValidationError(error);

    const {
      id,
      name,
      email,
      isAdmin,
      createdAt,
      updatedAt,
    } = await this.usecase.register(httpRequest.body);

    return ok({ id, name, email, createdAt, updatedAt, isAdmin });
  }

  async handleError(error: Error): Promise<HttpResponse> {
    if (error instanceof BodyValidationError) return badRequest(error);
    if (error instanceof UserAlreadyExistsError) return badRequest(error);
    return serverError(error);
  }
}
