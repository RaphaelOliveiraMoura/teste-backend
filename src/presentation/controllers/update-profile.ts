import { InvalidCredentialsError } from '@/domain/errors';
import {
  UpdateProfileUseCase,
  UpdateProfile,
  AuthorizationUseCase,
} from '@/domain/use-cases';
import {
  badRequest,
  Controller,
  HttpRequest,
  HttpResponse,
  HttpResponseError,
  ok,
  serverError,
  unauthorized,
} from '@/presentation/contracts';
import { BodyValidationError } from '@/presentation/errors';
import { Validation } from '@/validation/contracts';

type UpdateProfileControllerResponse = {
  id: string;
  name: string;
  email: string;
  createdAt: Date;
  updatedAt: Date;
  isAdmin: boolean;
};

export class UpdateProfileController implements Controller {
  constructor(
    private readonly usecase: UpdateProfileUseCase,
    private readonly validator: Validation,
    private readonly authenticator: AuthorizationUseCase
  ) {}

  async handle(
    httpRequest: HttpRequest<UpdateProfile.Params>
  ): Promise<
    HttpResponse<UpdateProfileControllerResponse | HttpResponseError>
  > {
    const { authorization } = httpRequest.headers;
    const user = await this.authenticator.authorize({ authorization });

    const error = this.validator.validate(httpRequest.body);

    if (error) throw new BodyValidationError(error);

    const { name, password } = httpRequest.body;

    const updatedUser = await this.usecase.update({
      id: user.id,
      name,
      password,
    });

    return ok({
      id: updatedUser.id,
      name: updatedUser.name,
      email: updatedUser.email,
      createdAt: updatedUser.createdAt,
      updatedAt: updatedUser.updatedAt,
      isAdmin: updatedUser.isAdmin,
    });
  }

  async handleError(error: Error): Promise<HttpResponse> {
    if (error instanceof InvalidCredentialsError) return unauthorized(error);
    if (error instanceof BodyValidationError) return badRequest(error);
    return serverError(error);
  }
}
