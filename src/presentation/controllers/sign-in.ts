import { InvalidCredentialsError } from '@/domain/errors';
import { SignInUseCase, SignIn } from '@/domain/use-cases';
import {
  badRequest,
  Controller,
  HttpRequest,
  HttpResponse,
  HttpResponseError,
  ok,
  serverError,
  unauthorized,
} from '@/presentation/contracts';
import { BodyValidationError } from '@/presentation/errors';
import { Validation } from '@/validation/contracts';

export class SignInController implements Controller {
  constructor(
    private readonly usecase: SignInUseCase,
    private readonly validator: Validation
  ) {}

  async handle(
    httpRequest: HttpRequest<SignIn.Params>
  ): Promise<HttpResponse<SignIn.Result | HttpResponseError>> {
    const error = this.validator.validate(httpRequest.body);

    if (error) throw new BodyValidationError(error);

    const { email, password } = httpRequest.body;

    const { token, name } = await this.usecase.signIn({
      email,
      password,
    });

    return ok({ token, email, name });
  }

  async handleError(error: Error): Promise<HttpResponse> {
    if (error instanceof InvalidCredentialsError) return unauthorized(error);
    if (error instanceof BodyValidationError) return badRequest(error);
    return serverError(error);
  }
}
