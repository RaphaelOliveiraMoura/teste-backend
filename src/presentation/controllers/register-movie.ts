import { InvalidAuthorizationError } from '@/domain/errors';
import {
  RegisterMovieUseCase,
  RegisterMovie,
  AuthorizationUseCase,
} from '@/domain/use-cases';
import {
  badRequest,
  Controller,
  HttpRequest,
  HttpResponse,
  HttpResponseError,
  ok,
  serverError,
  unauthorized,
} from '@/presentation/contracts';
import { BodyValidationError } from '@/presentation/errors';
import { Validation } from '@/validation/contracts';

export class RegisterMovieController implements Controller {
  constructor(
    private readonly usecase: RegisterMovieUseCase,
    private readonly validator: Validation,
    private readonly authenticator: AuthorizationUseCase
  ) {}

  async handle(
    httpRequest: HttpRequest<RegisterMovie.Params>
  ): Promise<HttpResponse<RegisterMovie.Result | HttpResponseError>> {
    const { authorization } = httpRequest.headers;

    const { isAdmin } = await this.authenticator.authorize({ authorization });

    const error = this.validator.validate(httpRequest.body);

    if (error) throw new BodyValidationError(error);

    const movie = await this.usecase.register({
      ...httpRequest.body,
      user: { isAdmin },
    });

    return ok(movie);
  }

  async handleError(error: Error): Promise<HttpResponse> {
    if (error instanceof InvalidAuthorizationError) return unauthorized(error);
    if (error instanceof BodyValidationError) return badRequest(error);
    return serverError(error);
  }
}
