import { InvalidAuthorizationError } from '@/domain/errors';
import {
  RegisterAdmin,
  DeactivateAccountUseCase,
  AuthorizationUseCase,
} from '@/domain/use-cases';
import {
  Controller,
  HttpRequest,
  HttpResponse,
  HttpResponseError,
  noBody,
  serverError,
  unauthorized,
} from '@/presentation/contracts';

export class DeactivateAccountController implements Controller {
  constructor(
    private readonly usecase: DeactivateAccountUseCase,
    private readonly authenticator: AuthorizationUseCase
  ) {}

  async handle(
    httpRequest: HttpRequest<RegisterAdmin.Params>
  ): Promise<HttpResponse<{} | HttpResponseError>> {
    const { authorization } = httpRequest.headers;

    const user = await this.authenticator.authorize({ authorization });

    await this.usecase.deactivate({ userId: user.id });

    return noBody();
  }

  async handleError(error: Error): Promise<HttpResponse> {
    if (error instanceof InvalidAuthorizationError) return unauthorized(error);
    return serverError(error);
  }
}
