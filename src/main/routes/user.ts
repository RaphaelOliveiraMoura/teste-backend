import { Router } from 'express';

import { ExpressRouteAdapter } from '@/main/adapters';
import {
  buildRegisterUserController,
  buildRegisterAdminController,
  buildDeactivateAccountController,
  buildUpdateProfileController,
  buildSignInController,
} from '@/main/factories';

export default function setup(router: Router) {
  router.post('/signin', ExpressRouteAdapter.adapt(buildSignInController()));
  router.post(
    '/register',
    ExpressRouteAdapter.adapt(buildRegisterUserController())
  );
  router.post(
    '/admin',
    ExpressRouteAdapter.adapt(buildRegisterAdminController())
  );
  router.delete(
    '/users/deactivate',
    ExpressRouteAdapter.adapt(buildDeactivateAccountController())
  );
  router.put(
    '/users',
    ExpressRouteAdapter.adapt(buildUpdateProfileController())
  );
}
