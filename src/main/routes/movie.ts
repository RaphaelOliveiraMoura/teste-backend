import { Router } from 'express';

import { ExpressRouteAdapter } from '@/main/adapters';
import {
  buildRegisterMovieController,
  buildListMoviesController,
  buildGetMovieDetailsController,
  buildRateMovieController,
} from '@/main/factories';

export default function setup(router: Router) {
  router.post(
    '/movies',
    ExpressRouteAdapter.adapt(buildRegisterMovieController())
  );
  router.get('/movies', ExpressRouteAdapter.adapt(buildListMoviesController()));
  router.get(
    '/movies/:movieId',
    ExpressRouteAdapter.adapt(buildGetMovieDetailsController())
  );
  router.post(
    '/movies/:movieId/rate',
    ExpressRouteAdapter.adapt(buildRateMovieController())
  );
}
