import { Request, Response } from 'express';

import { Controller } from '@/presentation/contracts';

export class ExpressRouteAdapter {
  static adapt(controller: Controller) {
    return async (req: Request, res: Response) => {
      try {
        const response = await controller.handle({
          body: req.body,
          headers: req.headers,
          params: req.params,
          query: req.query,
        });

        return res.status(response.statusCode).json(response.body);
      } catch (error) {
        const handledError = await controller.handleError(error);

        return res.status(handledError.statusCode).json(handledError.body);
      }
    };
  }
}
