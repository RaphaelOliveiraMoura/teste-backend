import { AuthorizationUseCase } from '@/domain/use-cases';
import { JWTHasher } from '@/infra/hasher';
import { UserRepository } from '@/infra/repositories';

export function buildAuthorizationService() {
  const hasher = new JWTHasher();
  const userRepository = new UserRepository();
  const authorizationService = new AuthorizationUseCase(userRepository, hasher);
  return authorizationService;
}
