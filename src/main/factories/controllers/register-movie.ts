import { RegisterMovieUseCase } from '@/domain/use-cases';
import { MovieRepository } from '@/infra/repositories';
import { buildAuthorizationService } from '@/main/factories/services/authorization';
import { RegisterMovieController } from '@/presentation/controllers';
import { ValidatorComposite } from '@/validation/composite';
import {
  RequiredValidator,
  ArrayValidator,
  DateValidator,
} from '@/validation/validators';

export function buildRegisterMovieController() {
  const repository = new MovieRepository();
  const usecase = new RegisterMovieUseCase(repository);
  const bodyValidator = new ValidatorComposite([
    new RequiredValidator('title'),
    new RequiredValidator('description'),
    new RequiredValidator('releaseDate'),
    new RequiredValidator('genre'),
    new RequiredValidator('actors'),
    new RequiredValidator('director'),
    new ArrayValidator('genre'),
    new ArrayValidator('actors'),
    new DateValidator('releaseDate'),
  ]);
  const controller = new RegisterMovieController(
    usecase,
    bodyValidator,
    buildAuthorizationService()
  );
  return controller;
}
