import { DeactivateAccountUseCase } from '@/domain/use-cases';
import { UserRepository } from '@/infra/repositories';
import { buildAuthorizationService } from '@/main/factories/services/authorization';
import { DeactivateAccountController } from '@/presentation/controllers';

export function buildDeactivateAccountController() {
  const repository = new UserRepository();
  const usecase = new DeactivateAccountUseCase(repository);
  const controller = new DeactivateAccountController(
    usecase,
    buildAuthorizationService()
  );
  return controller;
}
