import { UpdateProfileUseCase } from '@/domain/use-cases';
import { BcryptEncrypter } from '@/infra/encrypter';
import { UserRepository } from '@/infra/repositories';
import { buildAuthorizationService } from '@/main/factories/services/authorization';
import { UpdateProfileController } from '@/presentation/controllers';
import { ValidatorComposite } from '@/validation/composite';
import { PasswordValidator } from '@/validation/validators';

export function buildUpdateProfileController() {
  const repository = new UserRepository();
  const encrypter = new BcryptEncrypter();
  const usecase = new UpdateProfileUseCase(repository, encrypter);

  const bodyValidator = new ValidatorComposite([
    new PasswordValidator('password'),
  ]);

  const controller = new UpdateProfileController(
    usecase,
    bodyValidator,
    buildAuthorizationService()
  );
  return controller;
}
