import { RateMovieUseCase } from '@/domain/use-cases';
import { MovieRepository } from '@/infra/repositories';
import { buildAuthorizationService } from '@/main/factories/services/authorization';
import { RateMovieController } from '@/presentation/controllers';
import { ValidatorComposite } from '@/validation/composite';
import { RequiredValidator, OneOfTypeValidator } from '@/validation/validators';

export function buildRateMovieController() {
  const repository = new MovieRepository();
  const usecase = new RateMovieUseCase(repository);

  const bodyValidator = new ValidatorComposite([
    new RequiredValidator('rating'),
    new OneOfTypeValidator('rating', [0, 1, 2, 3, 4]),
  ]);

  const controller = new RateMovieController(
    usecase,
    bodyValidator,
    buildAuthorizationService()
  );

  return controller;
}
