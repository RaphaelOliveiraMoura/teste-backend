import { RegisterAdminUseCase } from '@/domain/use-cases';
import { BcryptEncrypter } from '@/infra/encrypter';
import { UserRepository } from '@/infra/repositories';
import { RegisterAdminController } from '@/presentation/controllers';
import { ValidatorComposite } from '@/validation/composite';
import {
  CompareFieldsValidator,
  RequiredValidator,
  EmailValidator,
  PasswordValidator,
} from '@/validation/validators';

export function buildRegisterAdminController() {
  const repository = new UserRepository();
  const encrypter = new BcryptEncrypter();
  const usecase = new RegisterAdminUseCase(repository, encrypter);
  const bodyValidator = new ValidatorComposite([
    new RequiredValidator('email'),
    new RequiredValidator('name'),
    new RequiredValidator('password'),
    new CompareFieldsValidator('password', 'confirmPassword'),
    new EmailValidator('email'),
    new PasswordValidator('password'),
  ]);
  const controller = new RegisterAdminController(usecase, bodyValidator);
  return controller;
}
