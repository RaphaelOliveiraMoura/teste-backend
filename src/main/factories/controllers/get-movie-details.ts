import { GetMovieDetailsUseCase } from '@/domain/use-cases';
import { MovieRepository } from '@/infra/repositories';
import { GetMovieDetailsController } from '@/presentation/controllers';

export function buildGetMovieDetailsController() {
  const repository = new MovieRepository();
  const usecase = new GetMovieDetailsUseCase(repository);
  const controller = new GetMovieDetailsController(usecase);
  return controller;
}
