import { SignInUseCase } from '@/domain/use-cases';
import { BcryptEncrypter } from '@/infra/encrypter';
import { JWTHasher } from '@/infra/hasher';
import { UserRepository } from '@/infra/repositories';
import { SignInController } from '@/presentation/controllers';
import { ValidatorComposite } from '@/validation/composite';
import {
  EmailValidator,
  PasswordValidator,
  RequiredValidator,
} from '@/validation/validators';

export function buildSignInController() {
  const userRepository = new UserRepository();
  const encrypter = new BcryptEncrypter();
  const hasher = new JWTHasher();

  const signInService = new SignInUseCase(userRepository, encrypter, hasher);

  const bodyValidator = new ValidatorComposite([
    new RequiredValidator('email'),
    new RequiredValidator('password'),
    new EmailValidator('email'),
    new PasswordValidator('password'),
  ]);

  const controller = new SignInController(signInService, bodyValidator);
  return controller;
}
