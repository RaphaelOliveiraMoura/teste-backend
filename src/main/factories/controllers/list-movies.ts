import { ListMoviesUseCase } from '@/domain/use-cases';
import { MovieRepository } from '@/infra/repositories';
import { ListMoviesController } from '@/presentation/controllers';

export function buildListMoviesController() {
  const repository = new MovieRepository();
  const usecase = new ListMoviesUseCase(repository);
  const controller = new ListMoviesController(usecase);
  return controller;
}
