import { RegisterUserUseCase } from '@/domain/use-cases';
import { BcryptEncrypter } from '@/infra/encrypter';
import { UserRepository } from '@/infra/repositories';
import { RegisterUserController } from '@/presentation/controllers';
import { ValidatorComposite } from '@/validation/composite';
import {
  CompareFieldsValidator,
  RequiredValidator,
  EmailValidator,
  PasswordValidator,
} from '@/validation/validators';

export function buildRegisterUserController() {
  const repository = new UserRepository();
  const encrypter = new BcryptEncrypter();
  const usecase = new RegisterUserUseCase(repository, encrypter);
  const bodyValidator = new ValidatorComposite([
    new RequiredValidator('email'),
    new RequiredValidator('name'),
    new RequiredValidator('password'),
    new CompareFieldsValidator('password', 'confirmPassword'),
    new EmailValidator('email'),
    new PasswordValidator('password'),
  ]);
  const controller = new RegisterUserController(usecase, bodyValidator);
  return controller;
}
