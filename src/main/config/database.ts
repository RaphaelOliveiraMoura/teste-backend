import { SequelizeDatabase } from '@/infra/sequelize';

export async function setupDatabase() {
  await SequelizeDatabase.initialize();
}
