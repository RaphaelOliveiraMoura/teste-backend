import { Application, Router } from 'express';

import movie from '@/main/routes/movie';
import user from '@/main/routes/user';

export function setupRoutes(app: Application) {
  const router = Router();

  user(router);
  movie(router);

  app.use(router);
}
