import { InvalidAuthorizationError } from '../errors';

import { MovieRepository } from '@/domain/contracts';
import { Actor, Director, Genre, Movie } from '@/domain/models';

export namespace RegisterMovie {
  export type Params = {
    title: string;
    description: string;
    genre: Genre[];
    actors: Actor[];
    releaseDate: Date;
    director: Director;
    user: {
      isAdmin: boolean;
    };
  };

  export type Result = Movie;
}

export class RegisterMovieUseCase {
  constructor(private readonly repository: MovieRepository) {}

  async register({
    user,
    ...movie
  }: RegisterMovie.Params): Promise<RegisterMovie.Result> {
    if (!user.isAdmin) {
      throw new InvalidAuthorizationError(
        'you dont have permission to this action'
      );
    }

    return this.repository.insert(movie);
  }
}
