import { UserRepository } from '@/domain/contracts';

export namespace DeactivateAccount {
  export type Params = {
    userId: string;
  };

  export type Result = boolean;
}

export class DeactivateAccountUseCase {
  constructor(private readonly userRepository: UserRepository) {}

  async deactivate({
    userId,
  }: DeactivateAccount.Params): Promise<DeactivateAccount.Result> {
    return this.userRepository.deactivate({ userId });
  }
}
