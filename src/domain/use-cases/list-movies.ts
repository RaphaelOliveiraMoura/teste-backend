import { MovieRepository } from '@/domain/contracts';
import { Genre, SanitizedMovie } from '@/domain/models';

export namespace ListMovies {
  export type Params = {
    filters: {
      title?: string;
      description?: string;
      genre?: Genre[];
      director?: string;
      actors?: string[];
    };
  };

  export type Result = SanitizedMovie[];
}

export class ListMoviesUseCase {
  constructor(private readonly movieRepository: MovieRepository) {}

  async get({ filters }: ListMovies.Params): Promise<ListMovies.Result> {
    return this.movieRepository.findAll({ filters });
  }
}
