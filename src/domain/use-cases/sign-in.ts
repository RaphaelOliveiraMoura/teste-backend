import { Encrypter, Hasher, UserRepository } from '@/domain/contracts';
import { InvalidCredentialsError } from '@/domain/errors';

export namespace SignIn {
  export type Params = {
    email: string;
    password: string;
  };

  export type Result = {
    token: string;
    email: string;
    name: string;
  };
}

export class SignInUseCase {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly encrypter: Encrypter,
    private readonly hasher: Hasher
  ) {}

  async signIn({ email, password }: SignIn.Params): Promise<SignIn.Result> {
    const user = await this.userRepository.findByEmail({ email });

    if (!user) throw new InvalidCredentialsError();

    if (user.deactivateAt) {
      throw new InvalidCredentialsError();
    }

    const correctPassword = await this.encrypter.compare(
      password,
      user.password
    );

    if (!correctPassword) throw new InvalidCredentialsError();

    const token = await this.hasher.encode({ id: user.id });

    return { token, email: user.email, name: user.name };
  }
}
