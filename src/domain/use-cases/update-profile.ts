import { Encrypter, UserRepository } from '@/domain/contracts';
import { User } from '@/domain/models';

export namespace UpdateProfile {
  export type Params = { id: string; name?: string; password?: string };

  export type Result = User;
}

export class UpdateProfileUseCase {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly encrypter: Encrypter
  ) {}

  async update({
    id,
    name,
    password,
  }: UpdateProfile.Params): Promise<UpdateProfile.Result> {
    const encryptedPassword = password
      ? await this.encrypter.encrypt(password)
      : undefined;

    const user = await this.userRepository.update({
      userId: id,
      userData: {
        name,
        password: encryptedPassword,
      },
    });

    return user;
  }
}
