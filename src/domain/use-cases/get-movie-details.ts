import { MovieNotExistsError } from '../errors';

import { MovieRepository } from '@/domain/contracts';
import { Movie } from '@/domain/models';

export namespace GetMovieDetails {
  export type Params = {
    movieId: string;
  };

  export type Result = Movie;
}

export class GetMovieDetailsUseCase {
  constructor(private readonly movieRepository: MovieRepository) {}

  async get({
    movieId,
  }: GetMovieDetails.Params): Promise<GetMovieDetails.Result> {
    const movie = await this.movieRepository.findById({ id: movieId });

    if (!movie) throw new MovieNotExistsError();

    return movie;
  }
}
