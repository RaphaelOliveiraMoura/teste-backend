import { Encrypter, UserRepository } from '@/domain/contracts';
import { UserAlreadyExistsError } from '@/domain/errors';
import { User } from '@/domain/models';

export namespace RegisterAdmin {
  export type Params = {
    email: string;
    name: string;
    password: string;
  };

  export type Result = User;
}

export class RegisterAdminUseCase {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly encrypter: Encrypter
  ) {}

  async register({
    email,
    name,
    password,
  }: RegisterAdmin.Params): Promise<RegisterAdmin.Result> {
    const alreadyExists = await this.userRepository.findByEmail({ email });

    if (alreadyExists) {
      throw new UserAlreadyExistsError();
    }

    const encryptedPassword = await this.encrypter.encrypt(password);

    return this.userRepository.insert({
      email,
      name,
      isAdmin: true,
      password: encryptedPassword,
    });
  }
}
