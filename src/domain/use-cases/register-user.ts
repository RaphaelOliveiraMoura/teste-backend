import { Encrypter, UserRepository } from '@/domain/contracts';
import { UserAlreadyExistsError } from '@/domain/errors';
import { User } from '@/domain/models';

export namespace RegisterUser {
  export type Params = {
    email: string;
    name: string;
    password: string;
  };

  export type Result = User;
}

export class RegisterUserUseCase {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly encrypter: Encrypter
  ) {}

  async register({
    name,
    email,
    password,
  }: RegisterUser.Params): Promise<RegisterUser.Result> {
    const alreadyExists = await this.userRepository.findByEmail({ email });

    if (alreadyExists) {
      throw new UserAlreadyExistsError();
    }

    const encryptedPassword = await this.encrypter.encrypt(password);

    return this.userRepository.insert({
      name,
      email,
      isAdmin: false,
      password: encryptedPassword,
    });
  }
}
