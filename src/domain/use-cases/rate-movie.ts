import { InvalidAuthorizationError, MovieNotExistsError } from '../errors';

import { MovieRepository } from '@/domain/contracts';
import { Movie, MovieRating } from '@/domain/models';

export namespace RateMovie {
  export type Params = {
    movieId: string;
    rating: MovieRating;
    user: {
      id: string;
      isAdmin: boolean;
    };
  };

  export type Result = Movie;
}

export class RateMovieUseCase {
  constructor(private readonly movieRepository: MovieRepository) {}

  async rate({
    movieId,
    user,
    rating,
  }: RateMovie.Params): Promise<RateMovie.Result> {
    const movie = await this.movieRepository.findById({ id: movieId });

    if (!movie) {
      throw new MovieNotExistsError();
    }

    if (user.isAdmin) {
      throw new InvalidAuthorizationError(
        'you dont have permission to this action'
      );
    }

    const updatedMovie = await this.movieRepository.rate({
      userId: user.id,
      movieId,
      rating,
    });

    return updatedMovie;
  }
}
