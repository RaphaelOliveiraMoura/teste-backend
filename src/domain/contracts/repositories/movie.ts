import { Genre, Movie, SanitizedMovie } from '@/domain/models';

export namespace MovieRepositoryParams {
  export type Insert = {
    title: string;
    description: string;
    genre: Genre[];
    actors: { id?: string; name: string }[];
    releaseDate: Date;
    director: { id?: string; name: string };
  };

  export type FindAll = {
    filters: {
      title?: string;
      description?: string;
      genre?: Genre[];
      director?: string;
      actors?: string[];
    };
  };

  export type FindById = { id: string };

  export type Rate = {
    userId: string;
    movieId: string;
    rating: number;
  };
}

export interface MovieRepository {
  insert: (params: MovieRepositoryParams.Insert) => Promise<Movie>;

  findAll: (params: MovieRepositoryParams.FindAll) => Promise<SanitizedMovie[]>;

  findById: (params: MovieRepositoryParams.FindById) => Promise<Movie | null>;

  rate: (params: MovieRepositoryParams.Rate) => Promise<Movie>;
}
