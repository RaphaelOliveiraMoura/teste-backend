import { User } from '@/domain/models';

export namespace UserRepositoryParams {
  export type Deactivate = { userId: string };

  export type Insert = {
    name: string;
    email: string;
    password: string;
    isAdmin: boolean;
  };

  export type FindByEmail = { email: string };

  export type FindById = { id: string };

  export type Update = {
    userId: string;
    userData: Partial<{
      name: string;
      password: string;
    }>;
  };
}

export interface UserRepository {
  deactivate: (params: UserRepositoryParams.Deactivate) => Promise<boolean>;

  insert: (params: UserRepositoryParams.Insert) => Promise<User>;

  findByEmail: (
    params: UserRepositoryParams.FindByEmail
  ) => Promise<User | null>;

  findById: (params: UserRepositoryParams.FindById) => Promise<User | null>;

  update: (params: UserRepositoryParams.Update) => Promise<User | null>;
}
