import { Actor } from './actor';
import { Director } from './director';
import { Genre } from './genre';
import { MovieRating } from './movie-rating';

export interface Movie {
  id: string;
  title: string;
  description: string;
  genre: Genre[];
  actors: Actor[];
  releaseDate: Date;
  ratingAvarage: MovieRating;
  director: Director;
  createdAt: Date;
  updatedAt: Date;
}

export type SanitizedMovie = Omit<
  Movie,
  'ratingAvarage' | 'actors' | 'description' | 'director'
>;
