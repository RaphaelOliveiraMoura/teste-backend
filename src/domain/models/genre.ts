export enum Genre {
  Action = 'Action',
  Adventure = 'Adventure',
  Comedy = 'Comedy',
  Crime = 'Crime',
  Mistery = 'Mistery',
  Fantasy = 'Fantasy',
  Historical = 'Historical',
  Horror = 'Horror',
  Romance = 'Romance',
  Satire = 'Satire',
  ScienceFiction = 'ScienceFiction',
  Other = 'Other',
}
