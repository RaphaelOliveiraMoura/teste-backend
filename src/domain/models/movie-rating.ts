export type MovieRating = 0 | 1 | 2 | 3 | 4;
