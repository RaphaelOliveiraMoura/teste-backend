export type Actor = {
  id: string;
  name: string;
};
