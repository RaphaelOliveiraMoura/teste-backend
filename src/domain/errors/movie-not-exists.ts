import { CustomError } from '@/utils';

export class MovieNotExistsError extends CustomError {
  constructor() {
    super(MovieNotExistsError, 'movie does not exists');
  }
}
