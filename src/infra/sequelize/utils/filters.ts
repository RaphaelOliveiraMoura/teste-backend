import { Op } from 'sequelize';

export const likeFilter = (filters) => (key: string) =>
  filters[key] && { [key]: { [Op.like]: `%${filters[key]}%` } };

export const combinedANDLikeFilter = (filters) => (key: string) => {
  if (!filters[key] || filters[key].length === 0) return null;

  const filtersArray = Array.isArray(filters[key])
    ? filters[key]
    : [filters[key]];

  return {
    [key]: {
      [Op.and]: filtersArray.map((value) => ({
        [Op.like]: `%${value}%`,
      })),
    },
  };
};

export const combinedORLikeFilter = (filters) => (key: string) => {
  if (!filters[key] || filters[key].length === 0) return null;

  const filtersArray = Array.isArray(filters[key])
    ? filters[key]
    : [filters[key]];

  return {
    [key]: {
      [Op.or]: filtersArray.map((value) => ({
        [Op.like]: `%${value}%`,
      })),
    },
  };
};

export const combineFilters = (whereFilters) =>
  [...whereFilters]
    .filter((w) => !!w)
    .reduce((acc, cur) => ({ ...acc, ...cur }), {});

export const filterActorsByName = (filters) => (movie) => {
  if (!filters.actors) return true;

  const actorNames = movie.actors.map(({ name }) => name);
  const filterActors = Array.isArray(filters.actors)
    ? filters.actors
    : [filters.actors];

  return filterActors.every((filterActor) =>
    actorNames.join(',').toLowerCase().includes(filterActor.toLowerCase())
  );
};
