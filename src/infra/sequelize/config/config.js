const path = require('path');

const dotenv = require('dotenv');

dotenv.config();

module.exports = {
  development: {
    dialect: 'sqlite',
    storage: path.resolve(__dirname, '..', 'database.sqlite'),
  },
  test: {
    dialect: 'sqlite',
    storage: path.resolve(__dirname, '..', 'database.sqlite'),
  },
  production: {
    dialect: 'mysql',
    username: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    host: process.env.MYSQL_HOST,
  },
};
