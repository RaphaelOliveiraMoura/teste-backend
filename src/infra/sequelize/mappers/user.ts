import { DataTypes, Sequelize } from 'sequelize';

import { MovieRatingModel } from '../models/movie-rating';
import { UserModel } from '../models/user';

export function init(sequelize: Sequelize) {
  UserModel.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: new DataTypes.STRING(128),
        allowNull: false,
      },
      email: {
        type: new DataTypes.STRING(128),
        allowNull: false,
      },
      password: {
        type: new DataTypes.STRING(128),
        allowNull: false,
      },
      isAdmin: {
        type: new DataTypes.CHAR(1),
        allowNull: false,
        defaultValue: '0',
        get() {
          return String(this.getDataValue('isAdmin')) === '1';
        },
        set(value) {
          this.setDataValue<any>('isAdmin', value ? '1' : '0');
        },
      },
      deactivateDate: {
        type: new DataTypes.DATE(),
        allowNull: true,
      },
    },
    {
      tableName: 'users',
      underscored: true,
      sequelize,
    }
  );

  UserModel.hasMany(MovieRatingModel, {
    as: 'ratings',
    foreignKey: 'user_id',
  });
}
