import { DataTypes, Sequelize } from 'sequelize';

import { MovieRatingModel } from '../models/movie-rating';

export function init(sequelize: Sequelize) {
  MovieRatingModel.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
      },
      rating: {
        type: new DataTypes.INTEGER(),
        allowNull: false,
      },
      movieId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'movie_id',
      },
      userId: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'user_id',
      },
    },
    {
      tableName: 'movie_rating',
      underscored: true,
      sequelize,
    }
  );
}
