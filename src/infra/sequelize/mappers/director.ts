import { DataTypes, Sequelize } from 'sequelize';

import { DirectorModel } from '../models/director';

export function init(sequelize: Sequelize) {
  DirectorModel.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: new DataTypes.STRING(128),
        allowNull: false,
      },
    },
    {
      tableName: 'directors',
      underscored: true,
      sequelize,
    }
  );
}
