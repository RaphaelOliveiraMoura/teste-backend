import { DataTypes, Sequelize } from 'sequelize';

import { ActorModel } from '../models/actor';
import { DirectorModel } from '../models/director';
import { MovieModel } from '../models/movie';
import { MovieRatingModel } from '../models/movie-rating';

export function init(sequelize: Sequelize) {
  MovieModel.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      title: {
        type: new DataTypes.STRING(128),
        allowNull: false,
      },
      description: {
        type: new DataTypes.STRING(128),
        allowNull: false,
      },
      releaseDate: {
        type: new DataTypes.DATE(),
        allowNull: false,
      },
      genre: {
        type: new DataTypes.STRING(128),
        allowNull: true,
      },
      directorId: {
        type: DataTypes.UUID,
        allowNull: true,
      },
    },
    {
      tableName: 'movies',
      underscored: true,
      sequelize,
    }
  );

  MovieModel.belongsToMany(ActorModel, {
    through: 'movie_actors',
    as: 'actors',
    foreignKey: 'movie_id',
    otherKey: 'actor_id',
  });

  MovieModel.belongsTo(DirectorModel, {
    as: 'director',
    foreignKey: 'director_id',
  });

  MovieModel.hasMany(MovieRatingModel, {
    as: 'ratings',
    foreignKey: 'movie_id',
  });
}
