import { init as actor } from './actor';
import { init as director } from './director';
import { init as movie } from './movie';
import { init as movieRating } from './movie-rating';
import { init as user } from './user';

export default [movieRating, user, actor, director, movie];
