import { DataTypes, Sequelize } from 'sequelize';

import { ActorModel } from '../models/actor';

export function init(sequelize: Sequelize) {
  ActorModel.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: new DataTypes.STRING(128),
        allowNull: false,
      },
    },
    {
      tableName: 'actors',
      underscored: true,
      sequelize,
    }
  );
}
