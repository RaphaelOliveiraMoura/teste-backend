import { v4 as uuid } from 'uuid';

import { UserModel } from '../models/user';

import { UserRepository, UserRepositoryParams } from '@/domain/contracts';
import { User } from '@/domain/models';

export class SequelizeUserRepository implements UserRepository {
  async deactivate({
    userId,
  }: UserRepositoryParams.Deactivate): Promise<boolean> {
    await UserModel.update(
      { deactivateDate: new Date() },
      { where: { id: userId } }
    );

    return true;
  }

  async insert({
    name,
    email,
    password,
    isAdmin,
  }: UserRepositoryParams.Insert): Promise<User> {
    const user = await UserModel.create({
      id: uuid(),
      name,
      email,
      password,
      isAdmin,
    });

    return UserModel.serialize(user);
  }

  async findByEmail({
    email,
  }: UserRepositoryParams.FindByEmail): Promise<User | null> {
    const user = await UserModel.findOne({
      where: { email, deactivateDate: null },
    });

    if (!user) return null;

    return UserModel.serialize(user);
  }

  async findById({ id }: UserRepositoryParams.FindById): Promise<User> {
    const user = await UserModel.findByPk(id);

    if (!user || !!user.deactivateDate) return null;

    return UserModel.serialize(user);
  }

  async update({
    userId,
    userData,
  }: UserRepositoryParams.Update): Promise<User> {
    const userDataWithoutUndefined = JSON.parse(JSON.stringify(userData));

    await UserModel.update(userDataWithoutUndefined, {
      where: { id: userId },
    });

    const user = this.findById({ id: userId });

    return user;
  }
}
