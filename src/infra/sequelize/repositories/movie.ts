import { Op } from 'sequelize';
import { v4 as uuid } from 'uuid';

import { ActorModel } from '../models/actor';
import { DirectorModel } from '../models/director';
import { MovieModel } from '../models/movie';
import { MovieRatingModel } from '../models/movie-rating';
import {
  combinedANDLikeFilter,
  combinedORLikeFilter,
  combineFilters,
  likeFilter,
  filterActorsByName,
} from '../utils/filters';

import { MovieRepository, MovieRepositoryParams } from '@/domain/contracts';
import { Genre, Movie, SanitizedMovie } from '@/domain/models';

export class SequelizeMovieRepository implements MovieRepository {
  async insert({
    title,
    description,
    releaseDate,
    director,
    actors,
    genre,
  }: MovieRepositoryParams.Insert): Promise<Movie> {
    const directorId = director.id || uuid();

    if (!director.id) {
      await DirectorModel.create({
        id: directorId,
        name: director.name,
      });
    }

    const movie = await MovieModel.create({
      id: uuid(),
      title,
      description,
      releaseDate,
      directorId,
      genre: genre.join(','),
    });

    const actorsWithId = await ActorModel.createMissingActors(actors);

    await movie.addActors(actorsWithId.map(({ id }) => id));

    return this.findById({ id: movie.id });
  }

  async findAll({
    filters = {},
  }: MovieRepositoryParams.FindAll): Promise<SanitizedMovie[]> {
    const where = combineFilters([
      likeFilter(filters)('title'),
      likeFilter(filters)('description'),
      combinedANDLikeFilter(filters)('genre'),
    ]);

    const directorIncludeFilter = {
      association: 'director',
      where: { name: { [Op.like]: `%${filters.director}%` } },
    };

    const actorsIncludeFilter = {
      association: 'actors',
      where: combineFilters([
        combinedORLikeFilter({ name: filters.actors })('name'),
      ]),
    };

    const include = [
      filters.director && directorIncludeFilter,
      filters.actors && actorsIncludeFilter,
    ].filter((i) => !!i);

    const movies = await MovieModel.findAll({
      where,
      include,
    });

    return movies.filter(filterActorsByName(filters)).map((movie) => ({
      id: movie.id,
      title: movie.title,
      description: movie.description,
      releaseDate: movie.releaseDate,
      genre: movie.genre.split(',') as Genre[],
      createdAt: movie.createdAt,
      updatedAt: movie.updatedAt,
    }));
  }

  async findById({ id }: MovieRepositoryParams.FindById): Promise<Movie> {
    const movie = await MovieModel.findByPk(id, { include: 'director' });

    if (!movie) return null;

    return MovieModel.serialize(movie);
  }

  async rate({
    movieId,
    rating,
    userId,
  }: MovieRepositoryParams.Rate): Promise<Movie> {
    const alreadyRate = await MovieRatingModel.findOne({
      where: { movieId, userId },
    });

    if (alreadyRate) {
      await MovieRatingModel.update({ rating }, { where: { movieId, userId } });
    } else {
      await MovieRatingModel.create({
        id: uuid(),
        rating,
        movieId,
        userId,
      });
    }

    return this.findById({ id: movieId });
  }
}
