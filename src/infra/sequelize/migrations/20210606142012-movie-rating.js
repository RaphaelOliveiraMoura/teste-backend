module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('movie_rating', {
      id: {
        type: Sequelize.DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
      },
      rating: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,
      },
      movie_id: {
        type: Sequelize.DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'movies',
          key: 'id',
        },
      },
      user_id: {
        type: Sequelize.DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      created_at: {
        type: Sequelize.DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.DataTypes.NOW,
      },
      updated_at: {
        type: Sequelize.DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.DataTypes.NOW,
      },
    }),
  down: (queryInterface, _Sequelize) =>
    queryInterface.dropTable('movie_rating'),
};
