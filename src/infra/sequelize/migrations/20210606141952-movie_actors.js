module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('movie_actors', {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      movie_id: {
        type: Sequelize.DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'movies',
          key: 'id',
        },
      },
      actor_id: {
        type: Sequelize.DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'actors',
          key: 'id',
        },
      },
      created_at: {
        type: Sequelize.DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.DataTypes.NOW,
      },
      updated_at: {
        type: Sequelize.DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.DataTypes.NOW,
      },
    }),
  down: (queryInterface, _Sequelize) =>
    queryInterface.dropTable('movie_actors'),
};
