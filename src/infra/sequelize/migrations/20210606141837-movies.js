module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('movies', {
      id: {
        type: Sequelize.DataTypes.UUID,
        primaryKey: true,
      },
      title: {
        type: new Sequelize.DataTypes.STRING(128),
        allowNull: false,
      },
      description: {
        type: new Sequelize.DataTypes.STRING(128),
        allowNull: false,
      },
      release_date: {
        type: new Sequelize.DataTypes.DATE(),
        allowNull: false,
      },
      genre: {
        type: new Sequelize.DataTypes.STRING(128),
        allowNull: true,
      },
      director_id: {
        type: Sequelize.DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'directors',
          key: 'id',
        },
      },
      created_at: {
        type: Sequelize.DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.DataTypes.NOW,
      },
      updated_at: {
        type: Sequelize.DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.DataTypes.NOW,
      },
    }),
  down: (queryInterface, _Sequelize) => queryInterface.dropTable('movies'),
};
