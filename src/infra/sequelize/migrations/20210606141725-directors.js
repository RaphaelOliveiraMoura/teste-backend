module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('directors', {
      id: {
        type: Sequelize.DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.DataTypes.STRING(128),
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.DataTypes.NOW,
      },
      updated_at: {
        type: Sequelize.DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.DataTypes.NOW,
      },
    }),
  down: (queryInterface, _Sequelize) => queryInterface.dropTable('directors'),
};
