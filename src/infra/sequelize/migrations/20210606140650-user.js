module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('users', {
      id: {
        type: Sequelize.DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
      },
      name: {
        type: Sequelize.DataTypes.STRING(128),
        allowNull: false,
      },
      email: {
        type: Sequelize.DataTypes.STRING(128),
        allowNull: false,
      },
      password: {
        type: Sequelize.DataTypes.STRING(128),
        allowNull: false,
      },
      is_admin: {
        type: Sequelize.DataTypes.CHAR(1),
        allowNull: false,
        defaultValue: '0',
      },
      deactivate_date: {
        type: Sequelize.DataTypes.DATE(),
        allowNull: true,
      },
      created_at: {
        type: Sequelize.DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.DataTypes.NOW,
      },
      updated_at: {
        type: Sequelize.DataTypes.DATE(),
        allowNull: false,
        defaultValue: Sequelize.DataTypes.NOW,
      },
    }),
  down: (queryInterface, _Sequelize) => queryInterface.dropTable('users'),
};
