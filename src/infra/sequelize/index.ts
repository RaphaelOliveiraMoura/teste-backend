import { resolve } from 'path';

import { Sequelize } from 'sequelize';
import Umzug from 'umzug';

import mappers from './mappers';

import { env } from '@/utils/env';

const config = require('./config/config.js');

const MIGRATIONS_FILES_MAP = true;

export class SequelizeDatabase {
  private static sequelize: Sequelize;

  private constructor() {}

  static db() {
    if (!this.sequelize) {
      throw new Error('Sequelize database not initialized');
    }

    return this.sequelize;
  }

  static async runMigrations(): Promise<void> {
    if (!MIGRATIONS_FILES_MAP) {
      await this.sequelize.sync({ force: true });
      return;
    }

    const umzug = new Umzug({
      migrations: {
        path: resolve(__dirname, 'migrations'),
        params: [
          this.sequelize.getQueryInterface(),
          this.sequelize.constructor,
        ],
      },
      storage: 'sequelize',
      storageOptions: {
        sequelize: this.sequelize,
      },
    });

    await umzug.down({ to: 0 });
    await umzug.up();
  }

  static async initialize() {
    this.sequelize = new Sequelize({
      ...config[env.environment],
      logging: false,
    });

    await this.sequelize.authenticate();

    mappers.map((init) => init(this.sequelize));

    await this.runMigrations();
  }
}
