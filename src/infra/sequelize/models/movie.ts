import {
  Association,
  HasManyAddAssociationsMixin,
  HasManyGetAssociationsMixin,
  Model,
  Optional,
} from 'sequelize';

import { ActorModel } from './actor';
import { DirectorModel } from './director';
import { MovieRatingModel } from './movie-rating';

import { Director, Genre, Movie, MovieRating } from '@/domain/models';

interface MovieAttributes {
  id: string;
  title: string;
  description: string;
  releaseDate: Date;
  genre: string;
  directorId: string;
}

interface MovieCreationAttributes extends Optional<MovieAttributes, 'id'> {}

export class MovieModel
  extends Model<MovieAttributes, MovieCreationAttributes>
  implements MovieAttributes {
  public id!: string;
  public title!: string;
  public description!: string;
  public genre!: string;
  public releaseDate!: Date;
  public directorId!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public readonly actors?: ActorModel[];

  public addActors!: HasManyAddAssociationsMixin<ActorModel, string>;
  public getActors!: HasManyGetAssociationsMixin<ActorModel>;

  public static associations: {
    actors: Association<MovieModel, ActorModel>;
    director: Association<MovieModel, DirectorModel>;
  };

  public static async calculateRatingAvarage({ movieId }) {
    const ratings = await MovieRatingModel.findAll({
      where: { movieId },
    });

    const ratingSum = ratings.reduce((acc, { rating }) => acc + rating, 0);
    const ratingLength = ratings.length;
    const ratingAvarage = ratingSum / ratingLength || 0;

    return ratingAvarage;
  }

  public static async serialize(movie: MovieModel): Promise<Movie> {
    const ratingAvarage = await MovieModel.calculateRatingAvarage({
      movieId: movie.id,
    });

    const actors = await movie.getActors();

    const director = (movie as any).director as Director;

    return {
      id: movie.id,
      title: movie.title,
      description: movie.description,
      releaseDate: movie.releaseDate,
      createdAt: movie.createdAt,
      updatedAt: movie.updatedAt,
      genre: movie.genre.split(',') as Genre[],
      ratingAvarage: ratingAvarage as MovieRating,
      director: {
        id: String(director.id),
        name: director.name,
      },
      actors: actors.map((actor) => ({
        id: String(actor.id),
        name: actor.name,
      })),
    };
  }
}
