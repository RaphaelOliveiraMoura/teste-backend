import { Model } from 'sequelize';

interface MovieRatingAttributes {
  id: string;
  rating: number;
  userId: string;
  movieId: string;
}

export class MovieRatingModel
  extends Model<MovieRatingAttributes, MovieRatingAttributes>
  implements MovieRatingAttributes {
  public id!: string;
  public rating!: number;
  public userId!: string;
  public movieId!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}
