import { Model, Optional } from 'sequelize';

import { User } from '@/domain/models';

interface UserAttributes {
  id: string;
  name: string;
  email: string;
  password: string;
  isAdmin: boolean;
  deactivateDate?: Date;
}

interface UserCreationAttributes
  extends Optional<UserAttributes, 'deactivateDate'> {}

export class UserModel
  extends Model<UserAttributes, UserCreationAttributes>
  implements UserAttributes {
  public id!: string;
  public name!: string;
  public email!: string;
  public password!: string;
  public isAdmin!: boolean;
  public deactivateDate!: Date;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public static serialize(user: UserModel): User {
    return {
      id: user.id,
      name: user.name,
      email: user.email,
      password: user.password,
      isAdmin: user.isAdmin,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
      deactivateAt: user.deactivateDate,
    };
  }
}
