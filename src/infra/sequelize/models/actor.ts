import { Model } from 'sequelize';
import { v4 as uuid } from 'uuid';

interface ActorAttributes {
  id: string;
  name: string;
}

export class ActorModel
  extends Model<ActorAttributes, ActorAttributes>
  implements ActorAttributes {
  public id!: string;
  public name!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public static async createMissingActors(
    actors: { id?: string; name: string }[]
  ) {
    const actorsToCreate = actors
      .filter((actor) => !actor.id)
      .map(({ name }) => ({ id: uuid(), name }));

    await ActorModel.bulkCreate(actorsToCreate);

    const actorsAlreadyCreated = actors.filter((actor) => !!actor.id);

    const actorsWithId = [...actorsToCreate, ...actorsAlreadyCreated];

    return actorsWithId;
  }
}
