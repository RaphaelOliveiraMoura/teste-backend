import { Model } from 'sequelize';

interface DirectorAttributes {
  id: string;
  name: string;
}

export class DirectorModel
  extends Model<DirectorAttributes, DirectorAttributes>
  implements DirectorAttributes {
  public id!: string;
  public name!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}
