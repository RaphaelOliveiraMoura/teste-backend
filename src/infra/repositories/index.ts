export { SequelizeUserRepository as UserRepository } from '../sequelize/repositories/user';
export { SequelizeMovieRepository as MovieRepository } from '../sequelize/repositories/movie';
