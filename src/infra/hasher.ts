import jwt, { SignOptions } from 'jsonwebtoken';

import { Hasher } from '@/domain/contracts';
import { env } from '@/utils';

const TWENTY_FOUR_HOURS = 24 * 60 * 60 * 60;

export class JWTHasher implements Hasher {
  private config: SignOptions = {
    expiresIn: TWENTY_FOUR_HOURS,
  };

  async encode(data: any): Promise<string> {
    const token = jwt.sign(data, env.hasher_secret, this.config);
    return token;
  }

  async decode(token: string): Promise<any> {
    const data = jwt.decode(token);
    return data;
  }
}
