import { Validation } from '@/validation/contracts';

export class OneOfTypeValidator implements Validation {
  constructor(
    private readonly fieldName: string,
    private readonly validValues: any[] = []
  ) {}

  validate(data: any): string {
    const value = data[this.fieldName];

    let isValid = false;

    this.validValues.forEach((validValue) => {
      if (validValue === value) isValid = true;
    });

    if (!isValid) {
      return `${this.fieldName} must be one of types [${this.validValues}]`;
    }

    return null;
  }
}
