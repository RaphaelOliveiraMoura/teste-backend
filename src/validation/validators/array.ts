import { Validation } from '@/validation/contracts';

export class ArrayValidator implements Validation {
  constructor(private readonly fieldName: string) {}

  validate(data: any): string {
    const value = data[this.fieldName];

    if (!value) return null;

    if (!Array.isArray(value)) return `${this.fieldName} must be array`;

    return null;
  }
}
