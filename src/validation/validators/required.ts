import { Validation } from '@/validation/contracts';

export class RequiredValidator implements Validation {
  constructor(private readonly fieldName: string) {}

  validate(data: any): string {
    const value = data[this.fieldName];
    if (value === 0) return null;
    if (!value) return `${this.fieldName} is required`;
    return null;
  }
}
